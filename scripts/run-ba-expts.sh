# Ensure that the settings file corresponds to the algorithm that you want to use for CTP computation.

for n in 100 200 300 400 500 600 700 800 900 1000
do
for i in 1 2 3
do
java -Xmx80G -jar eql.jar -c ba.settings -gq -i datasets/barabasi-albert-graph/m2/AB_Graph-$n-3-1with-kwds.nt -nk 2
java -Xmx80G -jar eql.jar -c ba.settings -gq -i datasets/barabasi-albert-graph/m3/AB_Graph-$n-3-1with-kwds.nt -nk 3
java -Xmx80G -jar eql.jar -c ba.settings -gq -i datasets/barabasi-albert-graph/m4/AB_Graph-$n-3-1with-kwds.nt -nk 4
done
done