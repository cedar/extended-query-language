for n in 1 2 3
do
 for sl in 1 2 4 6 8 10
 do
  for nk in 2 3 4 5 6 7 8 9 10
  do
    java -Xmx80G -jar eql.jar -c synrdf-small.settings -gq -i linegraph/segmentedlinegraph-numKeywords-$nk-segmentLength-$sl.nt
  done
 done
done

for n in 1 2 3
do
 for sl in 1 2 4 6 8 10
 do
  for nk in 2 3 4 5 6
  do
    java -Xmx80G -jar eql.jar -c synrdf-small.settings -gq -i comb/comb-numArms-$nk-segmentLength-$sl-numSegments-2-distanceBetweenArms-$sl.nt
  done
 done
done

for nk in 2 3 4 5 6 7 8 9 10
do
 for sl in 1 2 4 6 8 10
 do
  for n in 1 2 3
  do
    java -Xmx80G -jar eql.jar -c synrdf-small.settings -gq -i stargraph/segmentedstargraph-numKeywords-$nk-segmentLength-$sl.nt
  done
 done
done
